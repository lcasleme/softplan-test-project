﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Net.Http;

namespace API.TesteIntegracao.Controllers
{
    [Route("/")]
    [ApiController]
    public class BaseController : ControllerBase
    {
        internal HttpClient httpClient;
        internal readonly string urlDefault;

        public BaseController(IConfiguration config)
        {
            httpClient = new HttpClient();
            urlDefault = config.GetValue<string>("UrlAPITaxaJuros");
            httpClient.BaseAddress = new Uri(urlDefault);
        }
    }
}