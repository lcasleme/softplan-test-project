﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace API.TesteIntegracao.Controllers
{
    [ApiController]
    public class IntegracaoAPITaxaJurosController : BaseController
    {
        public IntegracaoAPITaxaJurosController(IConfiguration config) : base(config)
        {
        }

        [HttpGet, Route("teste-taxajuros")]
        public IActionResult TesteTaxaJurosUmPorcento()
        {
            decimal taxaJuros = JsonConvert.DeserializeObject<dynamic>(httpClient.GetAsync($"taxaJuros").Result.Content.ReadAsStringAsync().Result).retorno;

            if (taxaJuros == 0.01M)
                return Ok(taxaJuros);
            else
                return BadRequest();
        }

        [HttpGet, Route("teste-calculajuros")]
        public IActionResult TesteTaxaJurosUmPorcentoValorFixo()
        {
            decimal valor = JsonConvert.DeserializeObject<dynamic>(httpClient.GetAsync($"calculajuros?valorinicial=100&meses=5").Result.Content.ReadAsStringAsync().Result).retorno;

            if (valor == 105.10M)
                return Ok(valor);
            else
                return BadRequest();
        }
    }
}