﻿using Auxiliares.Modelos;
using System.Collections.Generic;

namespace Auxiliares.Interfaces
{
    /// <summary>
    /// Implementação baseado em:
    /// https://github.com/andrebaltieri/flunt/blob/master/Flunt/Notifications/Notifiable.cs
    /// Foi necessári a modificação para permitir que seja utilizado com interfaces
    /// </summary>
    internal interface INotificacao
    {
        void AddNotificacao(string property, string message);

        void AddNotificacao(Notificacao notificacao);

        void AddNotificacaos(IReadOnlyCollection<Notificacao> notificacaos);

        void AddNotificacoes(IList<Notificacao> notificacaos);

        void AddNotificacoes(ICollection<Notificacao> notificacaos);

        void AddNotificacoes(Notificador item);

        void AddNotificacoes(params Notificador[] items);

        void Limpar();
    }
}