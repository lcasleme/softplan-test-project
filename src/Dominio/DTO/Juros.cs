﻿namespace Dominio.DTO
{
    public class Juros
    {
        private const decimal TaxaJuros = 0.01M;

        public decimal ObterTaxaJuros()
        {
            return TaxaJuros;
        }
    }
}