﻿namespace Dominio.Modelos
{
    public class RetornoPadraoModelo
    {
        public string Rota { get; private set; }
        public string Parametros { get; private set; }
        public object Retorno { get; private set; }

        private void AdicionarRota(string rota)
        {
            Rota = rota;
        }

        private void AdicionarParametros(string parametros)
        {
            Parametros = parametros;
        }

        private void AdicionarRetorno(object retorno)
        {
            Retorno = retorno;
        }

        public RetornoPadraoModelo MontarRetornoPadraoModelo(string rota, string parametros, object retorno)
        {
            this.AdicionarRota(rota);
            this.AdicionarParametros(parametros);
            this.AdicionarRetorno(retorno);

            return this;
        }
    }
}