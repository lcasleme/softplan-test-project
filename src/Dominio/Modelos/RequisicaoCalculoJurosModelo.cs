﻿namespace Dominio.Modelos
{
    public class RequisicaoCalculoJurosModelo
    {
        public decimal ValorInicial { get; set; }
        public int Meses { get; set; }
    }
}