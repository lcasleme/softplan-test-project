﻿using Dominio.Modelos;

namespace Dominio.Interfaces
{
    public interface ICalculoTaxaJuros
    {
        decimal ObterCalculoJuros(RequisicaoCalculoJurosModelo requisicaoCalculoJurosModelo);
    }
}