﻿namespace Dominio.Interfaces
{
    public interface ITaxaJuros
    {
        decimal ObterTaxaJuros();
    }
}