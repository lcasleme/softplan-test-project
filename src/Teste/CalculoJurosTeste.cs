﻿using Teste.Fakes;
using Xunit;

namespace Teste
{
    public class CalculoJurosTeste
    {
        [Theory]
        [InlineData(100, 5, 105.10)]
        [InlineData(1000, 12, 1126.83)]
        [InlineData(8753.83, 55, 15131.21)]
        public void TotalDeveraSerUmPorcentoAoMes(decimal valor, int meses, decimal valorEsperado)
        {
            Assert.Equal(CalculoTaxaJurosFake.ObterCalculoJuros(valor, meses), valorEsperado);
        }
    }
}