using CalculoServico;
using Dominio.DTO;
using Dominio.Interfaces;
using Xunit;

namespace Teste
{
    public class TaxaJurosTeste
    {
        private readonly ITaxaJuros _taxaJuros;

        public TaxaJurosTeste()
        {
            _taxaJuros = new TaxaJuros(new Juros());
        }

        [Fact]
        public void JurosDeveraSerDeUmPorcento()
        {
            decimal porcentual = 0.01M;
            Assert.Equal(porcentual, _taxaJuros.ObterTaxaJuros());
        }

        [Fact]
        public void JurosNaoDeveraSerDiferenteDeUmPorcento()
        {
            decimal porcentual = 0.15M;
            Assert.False(porcentual.Equals(_taxaJuros.ObterTaxaJuros()));
        }

        [Fact]
        public void JurosNaoDeveraSerZero()
        {
            decimal porcentual = 0;
            Assert.False(porcentual.Equals(_taxaJuros.ObterTaxaJuros()));
        }
    }
}