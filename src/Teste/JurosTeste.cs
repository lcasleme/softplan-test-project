﻿using Dominio.DTO;
using Xunit;

namespace Teste
{
    public class JurosTeste
    {
        [Fact]
        public void JurosDeveraSerDeUmPorcento()
        {
            decimal porcentual = 0.01M;
            Assert.Equal(porcentual, new Juros().ObterTaxaJuros());
        }

        [Fact]
        public void JurosNaoDeveraSerDiferenteDeUmPorcento()
        {
            decimal porcentual = 0.15M;
            Assert.False(porcentual.Equals(new Juros().ObterTaxaJuros()));
        }

        [Fact]
        public void JurosNaoDeveraSerZero()
        {
            decimal porcentual = 0;
            Assert.False(porcentual.Equals(new Juros().ObterTaxaJuros()));
        }
    }
}