﻿using CalculoServico;
using Dominio.DTO;
using Dominio.Modelos;

namespace Teste.Fakes
{
    public static class CalculoTaxaJurosFake
    {
        public static decimal ObterCalculoJuros(decimal Valor, int QuantidadeMeses) => new CalculoTaxaJuros(new TaxaJuros(new Juros())).ObterCalculoJuros(new RequisicaoCalculoJurosModelo() { ValorInicial = Valor, Meses = QuantidadeMeses });
    }
}