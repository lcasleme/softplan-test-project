﻿using Dominio.Modelos;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [Route("/")]
    [ApiController]
    public class BaseController : ControllerBase
    {
        [NonAction]
        public IActionResult RetornoPadrao(object Retorno = null)
        {
            if (Retorno == null)
                return RetornoNulo();
            if (Retorno != null)
                return RetornoSucesso(Retorno);

            return RetornoNaoEncontrado();
        }

        [NonAction]
        public IActionResult RequisicaoInvalida()
        {
            return BadRequest();
        }

        [NonAction]
        private IActionResult RetornoNulo()
        {
            return NoContent();
        }

        [NonAction]
        private IActionResult RetornoSucesso(object Retorno)
        {
            return Ok(MontaObjetoRetorno(this.HttpContext, Retorno));
        }

        [NonAction]
        private IActionResult RetornoNaoEncontrado()
        {
            return NotFound(MontaObjetoRetorno(this.HttpContext, null));
        }

        private static object MontaObjetoRetorno(HttpContext httpContext, object Retorno = null)
        {
            return new RetornoPadraoModelo().MontarRetornoPadraoModelo(httpContext.Request.Path, httpContext.Request.QueryString.Value, Retorno);
        }
    }
}