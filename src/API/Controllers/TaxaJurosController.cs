﻿using Dominio.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [ApiController]
    public class TaxaJurosController : BaseController
    {
        [HttpGet]
        [Route("taxaJuros")]
        public IActionResult ObterTaxaJuros([FromServices] ITaxaJuros taxaJuros)
        {
            return RetornoPadrao(taxaJuros.ObterTaxaJuros());
        }
    }
}