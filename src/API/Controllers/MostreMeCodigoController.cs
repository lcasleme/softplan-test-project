﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace API.Controllers
{
    [Route("/")]
    [ApiController]
    public class MostreMeCodigoController : ControllerBase
    {
        private readonly IConfiguration _config;

        public MostreMeCodigoController(IConfiguration config)
        {
            _config = config;
        }

        [HttpGet] 
        [Route("showmethecode")]
        public IActionResult ShowMeTheCode()
        {
            return Ok(_config.GetValue<string>("GitLabURL"));
        }
    }
}