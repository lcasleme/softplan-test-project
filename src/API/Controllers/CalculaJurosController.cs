﻿using Dominio.Interfaces;
using Dominio.Modelos;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [Route("/")]
    [ApiController]
    public class CalculaJurosController : BaseController
    {
        [HttpGet]
        [Route("calculajuros")]
        public IActionResult CalcularJuros([FromServices] ICalculoTaxaJuros calculoTaxaJuros, [FromQuery] RequisicaoCalculoJurosModelo requisicaoCalculoJurosModelo)
        {
            if (requisicaoCalculoJurosModelo.ValorInicial == 0 || requisicaoCalculoJurosModelo.Meses == 0)
                return RequisicaoInvalida();

            return RetornoPadrao(calculoTaxaJuros.ObterCalculoJuros(requisicaoCalculoJurosModelo));
        }
    }
}