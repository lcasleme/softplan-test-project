﻿using CalculoServico;
using Dominio.DTO;
using Dominio.Interfaces;
using Microsoft.Extensions.DependencyInjection;

namespace API.IoC
{
    public static class InjecaoDeDependencia
    {
        public static void IniciarServicos(IServiceCollection services)
        {
            services.AddSingleton<Juros>();
            services.AddScoped<ITaxaJuros, TaxaJuros>();
            services.AddScoped<ICalculoTaxaJuros, CalculoTaxaJuros>();
        }
    }
}