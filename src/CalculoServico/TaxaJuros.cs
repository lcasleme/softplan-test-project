﻿using Dominio.DTO;
using Dominio.Interfaces;

namespace CalculoServico
{
    public class TaxaJuros : ITaxaJuros
    {
        private readonly Juros _juros;

        public TaxaJuros(Juros juros)
        {
            this._juros = juros;
        }

        public decimal ObterTaxaJuros()
        {
            try
            {
                return _juros.ObterTaxaJuros();
            }
            catch (System.Exception)
            {
                return decimal.Zero;
            }
        }
    }
}