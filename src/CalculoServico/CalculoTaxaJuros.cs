﻿using Dominio.Interfaces;
using Dominio.Modelos;
using System;

namespace CalculoServico
{
    public class CalculoTaxaJuros : ICalculoTaxaJuros
    {
        private readonly ITaxaJuros _taxaJuros;

        public CalculoTaxaJuros(ITaxaJuros taxaJuros)
        {
            _taxaJuros = taxaJuros;
        }

        public decimal ObterCalculoJuros(RequisicaoCalculoJurosModelo requisicaoCalculoJurosModelo)
        {
            try
            {
                if (requisicaoCalculoJurosModelo.ValorInicial == 0 || requisicaoCalculoJurosModelo.Meses == 0)
                    return decimal.Zero;

                decimal ValorFinal = Math.Round(((decimal)requisicaoCalculoJurosModelo.ValorInicial * (decimal)(Math.Pow((double)(1 + _taxaJuros.ObterTaxaJuros()), requisicaoCalculoJurosModelo.Meses))), 2);

                return ValorFinal;
            }
            catch (System.Exception)
            {
                return decimal.Zero;
            }
        }
    }
}